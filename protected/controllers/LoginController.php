<?php

class LoginController extends Controller
{

//	public function actionLogin()
//	{
//		$this->render('login');
//	}

    public $showDialog;
    public $dialogMsg;

    public function actionLogin()
    {
        $model       = new LoginForm;
        $accountform = new AccountForm();
        // if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax'] === 'login-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        $this->showDialog = true;
        $this->dialogMsg  = "Please enter your account.";
        // collect user input data
        if(isset($_POST['LoginForm']))
        {
            $model->attributes = $_POST['LoginForm'];

            $username = $model->username;
            $password = $model->password;

            $usernamelen = strlen($username);
            $passwordlen = strlen($password);

            $acct             = $accountform->checkusername($username,$password);
            $countacct        = count($acct);
            $this->showDialog = false;
            if($countacct > 0)
            {
                foreach($acct as $row)
                {
                    $aid           = $row['ID'];
                    $username      = $row['Username'];
                    $accounttypeid = $row['AccountType'];
                }
                if($passwordlen >= 8)
                {
                    //create a session
                    $session                           = new CHttpSession;
                    $session->open();
                    $session->clear();
                    $session_id                        = session_id();
                    $session->setSessionID($session_id);
                    Yii::app()->session['AID']         = $aid;
                    Yii::app()->session['SessionID']   = $session_id;
                    Yii::app()->session['AccountType'] = $accounttypeid;
                    Yii::app()->session['username']    = $username;
                    
                    $this->redirect(array('/site/index'));
                }
                else
                {
                    $this->showDialog = true;
                    $this->dialogMsg  = "Please enter your password. Minimum of 8 alphanumeric.";
                }
            }
            else
            {
                $this->showDialog = true;
                $this->dialogMsg  = "Invalid username or password, Please try again";
            }
        }

        // display the login form
        $this->render('login',array('model'=>$model));
    }

//    public function actionLogin()
//    {
//        $model = new LoginForm;
//
//        $this->showDialog = true;
//        $this->dialogMsg  = "This is WidgetDialog";
//
//        // if it is ajax validation request
//        if(isset($_POST['ajax']) && $_POST['ajax'] === 'login-form')
//        {
//            echo CActiveForm::validate($model);
//            Yii::app()->end();
//        }
//
//        // collect user input data
//        if(isset($_POST['LoginForm']))
//        {
//            $model->attributes = $_POST['LoginForm'];
//            // validate user input and redirect to the previous page if valid
//            if($model->validate() && $model->login())
//                $this->redirect(Yii::app()->user->returnUrl);
//        }
//        // display the login form
//        $this->render('login',array('model'=>$model));
//    }
    // Uncomment the following methods and override them if needed
    /*
      public function filters()
      {
      // return the filter configuration for this controller, e.g.:
      return array(
      'inlineFilterName',
      array(
      'class'=>'path.to.FilterClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }

      public function actions()
      {
      // return external action classes, e.g.:
      return array(
      'action1'=>'path.to.ActionClass',
      'action2'=>array(
      'class'=>'path.to.AnotherActionClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }
     */
}